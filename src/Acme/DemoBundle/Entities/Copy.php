<?php
namespace Acme\DemoBundle\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Copy
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="copy_sequence", initialValue=1, allocationSize=10)
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $innerNumber;

    /**
     * @ORM\ManyToOne(targetEntity="Book", inversedBy="copies" )
     **/
    private $book;

    public function getId(){
        return $this->id;
    }
    public function getInnerNumber(){
        return $this->innerNumber;
    }
    public function setInnerNumber($innerNumber){
        $this->innerNumber  =   $innerNumber;
    }

    public function getBook(){
        return $this->book;
    }
    public function setBook(Book $book = null){
        $this->book = $book;
    }

    /**
     * Override toString() method to return the name of the group
     * @return string name
     */
    public function __toString()
    {
        return $this->getInnerNumber();
    }
}
