<?php
namespace Acme\DemoBundle\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


//название, автор, кол-во страниц

/**
* @ORM\Entity
*/
class Book
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="book_sequence", initialValue=1, allocationSize=10)
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     **/
    private $title;

    /**
     * @ORM\Column(type="string")
     **/
    private $edition;

    /**
     * @ORM\Column(type="integer")
     **/
    private $volume;

    /**
     *
     * @ORM\OneToMany(targetEntity="Copy", mappedBy="book", cascade={"remove"})
     */
    private $copies;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Person", inversedBy="books")
     */
    private $authors;

    public function __construct() {
        $this->copies   = new ArrayCollection();
        $this->authors  = new ArrayCollection();
    }

    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }
    public function setTitle($title){
        $this->title=$title;
    }

    public function getEdition(){
        return $this->edition;
    }
    public function setEdition($edition){
        $this->edition = $edition;
    }

    public function getVolume(){
        return $this->volume;
    }
    public function setVolume($volume){
        $this->volume = $volume;
    }

    public function getCopies(){
        return $this->copies;
    }

    public function getAuthors(){
        return $this->authors;
    }
    public function setAuthors(ArrayCollection $authors = null){
        $this->authors = $authors;
    }

    /*
     * A "__toString()" method was not found on the objects of type "Acme\DemoBundle\Entities\Book" passed to the choice field. To read a custom getter instead, set the option "property" to the desired property path.
     */
    /**
     * Override toString() method to return the name of the group
     * @return string name
     */
    public function __toString()  //property либо тогда указывать.
    {
        return $this->getTitle();
    }
}
