<?php
namespace Acme\DemoBundle\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Position
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="position_sequence", initialValue=1, allocationSize=10)
     */
    private $id;

    /**
     * @ORM\Column(type="string",unique=true)
     */
    private $title;

    /**
    * @ORM\ManyToMany(targetEntity="Person", mappedBy="positions")
    */
    private $persons;

    public function __construct() {
        $this->persons  = new ArrayCollection();
    }

    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }
    public function setTitle($title){
        $this->title = $title;
    }

    public function getPersons(){
        return $this->persons;
    }

    /**
     * Override toString() method to return the name of the group
     * @return string name
     */

    public function __toString()
    {
        return $this->title;
    }

}

