<?php
namespace Acme\DemoBundle\Entities;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="person_sequence", initialValue=1, allocationSize=10)
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     */
    private $surname;
    /**
     * @ORM\ManyToMany(targetEntity="Position", inversedBy="persons" )
     */
    private $positions;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $cardnumber = null;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="authors")
     */
    private $books;

    public function __construct() {
        $this->positions    = new ArrayCollection();
        $this->books        = new ArrayCollection();
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getSurname(){
        return $this->surname;
    }
    public function setSurname($surname){
        $this->surname = $surname;
    }

    public function getPositions(){
        return $this->positions;
    }
    public function setPositions(ArrayCollection $positions = null){
        $this->positions = $positions;
    }

    public function setCardnumber($cardnumber){
        $this->cardnumber = $cardnumber;
    }
    public function getCardnumber(){
        return $this->cardnumber;
    }

    public function getBooks(){
        return $this->books;
    }


    /**
    * Override toString() method to return the name of the group
    * @return string name
    */
    public function __toString()
    {
        return $this->getName()." ".$this->getSurname();
    }
}