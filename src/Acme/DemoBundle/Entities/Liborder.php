<?php
namespace Acme\DemoBundle\Entities;

use \DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Acme\DemoBundle\Entities\Copy;
use Acme\DemoBundle\Entities\Person;


/**
 * @ORM\Entity
 */
class Liborder
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="liborder_sequence", initialValue=1, allocationSize=10)
     */
    private $id;

    /** @ORM\Column(type="datetime")
    */
    private $stdate;

    /** @ORM\Column(type="datetime")
     */
    private $findate;

    /**
     * * Односторонняя связь
     * @ORM\ManyToOne(targetEntity="Book" )
     **/

    private $book;

    /**
     * * Односторонняя связь
     * @ORM\ManyToOne(targetEntity="Copy" )
     **/

    private $copy;

    /**
     * Односторонняя связь
     * @ORM\ManyToOne(targetEntity="Person" )
     **/
    private $person;

    public function getId(){
        return $this->id;
    }

    public function getCopy(){
        return $this->copy;
    }
    public function setCopy(Copy $copy = null){
        $this->copy = $copy;
    }

    public function getBook(){
        return $this->book;
    }
    public function setBook(Book $book = null){
        $this->book = $book;
    }

    public function getPerson(){
        return $this->person;
    }
    public function setPerson(Person $person = null){
        $this->person = $person;
    }

    public function setStdate(DateTime $stdate = null){
        $this->stdate = $stdate;
    }
    public function getStdate(){
        return $this->stdate;
    }

    public function setFindate(DateTime $findate = null){
        $this->findate = $findate;
    }
    public function getFindate(){
        return $this->findate;
    }

    /**
     * Override toString() method to return the name of the group
     * @return string name
     */
    public function __toString()
    {
        $result = "Абонент: ".$this->getPerson()->getSurname()." ".$this->getPerson()->getName()." ";
        $result = $result."Название книги: ".$this->getCopy()->getBook()->getTitle()." ";
        $result = $result."Номер копии: ".$this->getCopy()->getInnerNumber();
        return $result;
    }
}