<?php

namespace Acme\DemoBundle\Controllers;

use Acme\DemoBundle\Forms\ChoosePersonType;
use Acme\DemoBundle\Forms\DeleteCopyType;
use \DateTime;
use \Exception;
use Acme\DemoBundle\Entities\Book;
use Acme\DemoBundle\Entities\Copy;
use Acme\DemoBundle\Entities\Order;
use Acme\DemoBundle\Entities\Person;
use Symfony\Component\Form\FormEvent;
use Acme\DemoBundle\Entities\Liborder;
use Symfony\Component\Form\FormEvents;
use Acme\DemoBundle\Entities\Position;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Acme\DemoBundle\Forms\PositionType;
use Acme\DemoBundle\Forms\LiborderType;
use Acme\DemoBundle\Forms\PersonType;
use Acme\DemoBundle\Forms\BookType;
use Acme\DemoBundle\Forms\DeleteBookType;
use Acme\DemoBundle\Forms\CopyType;
use Acme\DemoBundle\Forms\AuthorType;
use Acme\DemoBundle\Forms\DeletePersonType;
use Acme\DemoBundle\Forms\FinLiborderType;
use Symfony\Component\Form\FormError;

class BookController
{
    const BOOK_NOT_FOUND_ERROR              = "Не найдена книга с данным идентификатором.";
    const BOOK_ALREADY_EXISTS               = "Данная книга уже зарегистрирована в системе.";
    const COPY_NOT_FOUND_ERROR              = "Не найдена копия с данным идентификатором.";
    const COPY_ON_HANDS_ERROR               = "Копия с данным идентификатором выдана на руки в данный момент.";
    const BOOK_DO_NOT_HAVE_THIS_COPY_ERROR  = "Книга не имеет копии с данным идентификатором.";
    const COPY_ALLREADY_EXITS               = "Копия с данным номером уже зарегистрирована в системе.";
    const BOOK_HAS_COPY_ON_HANDS_ERROR      = "Книга имеет копию выданную на руки в данный момент.";

    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    private function getErrors($form)
    {
        $errors = array();
        if ($form instanceof \Symfony\Component\Form\Form) {

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }

            foreach ($form->all() as $key => $child) {

                if ($err = $this->getErrors($child)) {
                    $errors[$key] = $err;
                }
            }
        }
        return $errors;
    }

    public function get_books_and_copies()
    {
        $books = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->findAll();

        //действия с книгами
        $hidden_form_add_book = $this->app['form.factory']->createBuilder(new BookType($this->app['em'], $this->app['url_generator']) )->getForm();
        $hidden_form_edit_book = $this->app['form.factory']->createBuilder(new BookType($this->app['em'], $this->app['url_generator'], true))->getForm();
        $hidden_form_delete_book = $this->app['form.factory']->createBuilder(new DeleteBookType())->getForm();

        //действия с копиями
        $hidden_form_add_copy_popup = $this->app['form.factory']->createBuilder(new CopyType())->getForm();
        $hidden_form_edit_copy_popup = $this->app['form.factory']->createBuilder(new CopyType(true))->getForm();
        $hidden_form_delete_copy_popup = $this->app['form.factory']->createBuilder(new DeleteCopyType())->getForm();

        return $this->app['twig']->render(
            'books.html.twig',
            array(
                'books' => $books,
                'hidden_form_add_book' => $hidden_form_add_book->createView(),
                'hidden_form_edit_book' => $hidden_form_edit_book->createView(),
                'hidden_form_delete_book' => $hidden_form_delete_book->createView(),

                'hidden_form_add_copy_popup' => $hidden_form_add_copy_popup->createView(),
                'hidden_form_edit_copy_popup' => $hidden_form_edit_copy_popup->createView(),
                'hidden_form_delete_copy_popup' => $hidden_form_delete_copy_popup->createView()
            )
        );
    }


    public function BookAllReadyExist(Book $book) {

        $query = $this->app['em']->createQuery('SELECT COUNT(b) FROM Acme\DemoBundle\Entities\Book b WHERE ( LOWER( b.title) = LOWER (:title)
                                                              AND LOWER (b.edition) = LOWER(:edition) AND b.volume = :volume )');

        $query->setParameter("title",   $book->getTitle());
        $query->setParameter("edition", $book->getEdition());
        $query->setParameter("volume",  $book->getVolume());

        $count = $query->getSingleScalarResult();

        if ($count > 0) {
            return true;
        }   else {
            return false;
        }
    }

    public function CopyAllReadyExist( Book $book, Copy $copy) {

        $copy_query = $this->app['em']->createQuery("SELECT COUNT(c)
                                                  FROM Acme\DemoBundle\Entities\Copy c
                                                  WHERE LOWER(c.innerNumber) = LOWER(:innerNumber) and c.book=:book_id ");

        $copy_query->setParameter("innerNumber", $copy->getInnerNumber());
        $copy_query->setParameter("book_id", $book->getId());

        $count = $copy_query->getSingleScalarResult();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function add_book(Request $request)
    {
        $book = new Book();
        $form =  $this->app['form.factory']->createBuilder(new BookType($this->app['em'], $this->app['url_generator']), $book)->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ( ! $this->BookAllReadyExist($book) ) {
                $this->app['em']->persist($book);
                $this->app['em']->flush();

                $array = array();
                $array['id'] = $book->getId();
                $array['title'] = $book->getTitle();
                $array['edition'] = $book->getEdition();
                $array['volume'] = $book->getVolume();

                foreach ($book->getAuthors() as $key => $author) {
                    $array['authors'][$key]['name'] = $author->getName();
                    $array['authors'][$key]['surname'] = $author->getSurname();
                }
                return $this->app->json($array, 200);
            } else {
                $errors = array();
                $errors["form"] = self::BOOK_ALREADY_EXISTS;
                return $this->app->json($errors, 400);
            }
        } else {
            return $this->app->json($this->getErrors($form), 400);
        }
    }

    public function edit_book(Request $request, $book_id)
    {
        $book = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->find($book_id);

        if ($book) {
            $form = $this->app['form.factory']->createBuilder(new BookType($this->app['em'],$this->app['url_generator'], true), $book)->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {
                if ( ! $this->BookAllReadyExist($book) ) {
                    $this->app['em']->flush();
                    $array = array();
                    $array['id'] = $book->getId();
                    $array['title'] = $book->getTitle();
                    $array['volume'] = $book->getVolume();
                    $array['edition'] = $book->getEdition();

                    foreach ($book->getAuthors() as $key => $author) {
                        $array['authors'][$key]['name'] = $author->getName();
                        $array['authors'][$key]['surname'] = $author->getSurname();
                    }

                    return $this->app->json($array, 200);
                } else {
                    $errors = array();
                    $errors["form"] = self::BOOK_ALREADY_EXISTS;
                    return $this->app->json($errors, 400);
                }
            } else {
                return $this->app->json($this->getErrors($form), 400);
            }
        } else {
            $errors = array();
            $errors["form"] = self::BOOK_NOT_FOUND_ERROR;
            return $this->app->json($errors, 400);
        }
    }


    public function delete_book(Request $request, $book_id)
    {

        $book = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->find($book_id);

        if ($book) {
            $query = $this->app['em']->createQuery('SELECT COUNT (l) FROM Acme\DemoBundle\Entities\Liborder l WHERE (
                                                     l.copy IN (
                                                      SELECT copy FROM Acme\DemoBundle\Entities\Copy copy WHERE copy.book = :book_id))');

            $query->setParameter("book_id", $book_id);
            $count = $query->getSingleScalarResult();

            if ($count == 0) {
                $form = $this->app['form.factory']->createBuilder(new DeleteBookType())->getForm();
                $form->handleRequest($request);

                if ($form->isValid()) {

                    $this->app['em']->remove($book);
                    $this->app['em']->flush();

                    $array = array();
                    $array['book_id'] = $book_id;

                    return $this->app->json($array, 200);

                } else {
                    return $this->app->json($this->getErrors($form), 400);
                }
            } else {

                $errors = array();
                $errors["form"] = self::BOOK_HAS_COPY_ON_HANDS_ERROR;

                return $this->app->json($errors, 400);
            }

        } else {

            $errors = array();
            $errors["form"] = self::BOOK_NOT_FOUND_ERROR;

            return $this->app->json($errors, 400);
        }
    }


    public function add_copy(Request $request, $book_id)
    {

        $book = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->find($book_id);

        if ($book) {
            $copy = new Copy();
            $form = $this->app['form.factory']->createBuilder(new CopyType(), $copy)->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {

                if (! $this->CopyAllReadyExist($book, $copy)) {
                    $book->getCopies()->add($copy);
                    $copy->setBook($book);

                    $this->app['em']->persist($copy);
                    $this->app['em']->flush();

                    $array = array();
                    $array['copy_id'] = $copy->getId();
                    $array['book_id'] = $book->getId();
                    $array['inner_number'] = $copy->getInnerNumber();

                    return $this->app->json($array, 200);

                } else {

                    $errors = array();
                    $errors["form"] = self::COPY_ALLREADY_EXITS;
                    return $this->app->json($errors, 400);
                }
            } else {
                return $this->app->json($this->getErrors($form), 400);
            }

        } else {
            $errors = array();
            $errors["form"] = self::BOOK_NOT_FOUND_ERROR;

            return $this->app->json($errors, 400);
        }

    }

    public function edit_copy(Request $request, $book_id, $copy_id)
    {

        $book = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->find($book_id);
        $copy = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Copy")->find($copy_id);


        if ($book) {
            if ($copy) {
                if ($copy->getBook()->getId() == $book_id and
                    $book->getCopies()->contains($copy)
                ) {

                    $form = $this->app['form.factory']->createBuilder(new CopyType(true), $copy)->getForm();
                    $form->handleRequest($request);

                    if ($form->isValid()) {

                        if ( ! $this->CopyAllReadyExist($book, $copy)) {
                            $this->app['em']->flush();

                            $array = array();
                            $array['copy_id'] = $copy->getId();
                            $array['innerNumber'] = $copy->getInnerNumber();
                            $array['book_id'] = $book_id;

                            return $this->app->json($array, 200);

                        } else {
                            $errors = array();
                            $errors["form"] = self::COPY_ALLREADY_EXITS;
                            return $this->app->json($errors, 400);
                        }

                    } else {
                        return $this->app->json($this->getErrors($form), 400);
                    }

                } else {

                    $errors = array();
                    $errors["form"] = self::BOOK_DO_NOT_HAVE_THIS_COPY_ERROR;

                    return $this->app->json($errors, 400);
                }
            } else {

                $errors = array();
                $errors["form"] = self::COPY_NOT_FOUND_ERROR;

                return $this->app->json($errors, 400);
            }
        } else {

            $errors = array();
            $errors["form"] = self::BOOK_NOT_FOUND_ERROR;

            return $this->app->json($errors, 400);
        }
    }

    public function delete_copy(Request $request, $book_id, $copy_id)
    {

        $book = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->find($book_id);
        $copy = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Copy")->find($copy_id);

        if ($book) {
            if ($copy) {
                if ($copy->getBook()->getId() == $book_id and
                    $book->getCopies()->contains($copy)
                ) {
                    $query = $this->app['em']->createQuery('SELECT COUNT(l) FROM Acme\DemoBundle\Entities\Liborder l
                                                                WHERE l.copy = :copy_id ');

                    $query->setParameter("copy_id", $copy_id);
                    $count = $query->getSingleScalarResult();

                    if ($count == 0) {
                        $form = $this->app['form.factory']->createBuilder(new DeleteCopyType())->getForm();
                        $form->handleRequest($request);
                        if ($form->isValid()) {

                            $this->app['em']->remove($copy);
                            $this->app['em']->flush();

                            $array = array();
                            $array['copy_id'] = $copy_id;
                            $array['book_id'] = $book_id;

                            return $this->app->json($array, 200);

                        } else {
                            return $this->app->json($this->getErrors($form), 400);
                        }

                    } else {
                        $errors = array();
                        $errors["form"] = self::COPY_ON_HANDS_ERROR;
                        return $this->app->json($errors, 400);
                    }
                } else {

                    $errors = array();
                    $errors["form"] = self::BOOK_DO_NOT_HAVE_THIS_COPY_ERROR;

                    return $this->app->json($errors, 400);
                }
            } else {

                $errors = array();
                $errors["form"] = self::COPY_NOT_FOUND_ERROR;

                return $this->app->json($errors, 400);
            }
        } else {

            $errors = array();
            $errors["form"] = self::BOOK_NOT_FOUND_ERROR;

            return $this->app->json($errors, 400);
        }
    }


    public function get_book(Request $request)
    {

        $book_id = $request->get('book_id');
        $book = $this->app['em']->find('Acme\DemoBundle\Entities\Book', $book_id);

        if ($book) {
            return $this->app['twig']->render(
                'book.html.twig',
                array(
                    'book' => $book,
                    'copies' => $book->getCopies(),
                )
            );
        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::BOOK_NOT_FOUND_ERROR,
                )
            );
        }
    }

    public function get_copy(Request $request, $book_id, $copy_id ){

        $book = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Book")->find($book_id);
        $copy = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Copy")->find($copy_id);

        if ($book) {
            if ($copy) {
                if ($copy->getBook()->getId() == $book_id and
                    $book->getCopies()->contains($copy)
                ) {
                    return  $this->app['twig']->render(
                        'copy.html.twig',
                        array(
                            'copy' => $copy,
                        )
                    );
                } else  {

                    return  $this->app['twig']->render(
                        'error.html.twig',
                        array(
                            'error' => self::BOOK_DO_NOT_HAVE_THIS_COPY_ERROR,
                        )
                    );
                }

            } else {

                return  $this->app['twig']->render(
                    'error.html.twig',
                    array(
                        'error' => self::COPY_NOT_FOUND_ERROR,
                    )
                );
            }
        } else {
            return  $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::BOOK_NOT_FOUND_ERROR,
                )
            );
        }
    }
}