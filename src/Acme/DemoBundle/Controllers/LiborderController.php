<?php
namespace Acme\DemoBundle\Controllers;

use Acme\DemoBundle\Forms\ChoosePersonType;
use Acme\DemoBundle\Forms\DeleteCopyType;
use \DateTime;
use \Exception;
use Acme\DemoBundle\Entities\Book;
use Acme\DemoBundle\Entities\Copy;
use Acme\DemoBundle\Entities\Order;
use Acme\DemoBundle\Entities\Person;
use Symfony\Component\Form\FormEvent;
use Acme\DemoBundle\Entities\Liborder;
use Symfony\Component\Form\FormEvents;
use Acme\DemoBundle\Entities\Position;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Acme\DemoBundle\Forms\PositionType;
use Acme\DemoBundle\Forms\LiborderType;
use Acme\DemoBundle\Forms\PersonType;
use Acme\DemoBundle\Forms\BookType;
use Acme\DemoBundle\Forms\DeleteBookType;
use Acme\DemoBundle\Forms\CopyType;
use Acme\DemoBundle\Forms\AuthorType;
use Acme\DemoBundle\Forms\DeletePersonType;
use Acme\DemoBundle\Forms\FinLiborderType;
use Symfony\Component\Form\FormError;

class LiborderController
{

    const SUCCESS               = "Информация о заявке успешно сохранена";

    private $app;


    public function __construct($app)
    {
        $this->app = $app;
    }

    public function new_order(Request $request)
    {
        $order      = new Liborder();
        $now        = new DateTime('now');


        $order->setStdate($now);

        $form = $this->app['form.factory']->createBuilder(new LiborderType($this->app['em']), $order)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->app['em']->persist($order);
            $this->app['em']->flush();

            return $this->app['twig']->render(
                'success.html.twig',
                array(
                    'message' => self::SUCCESS,
                )
            );

        } else {
            return $this->app['twig']->render(
                'new_order.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function finish_order(Request $request)
    {

        $form = $this->app['form.factory']->createBuilder(new FinLiborderType($this->app['em']))->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            $form_data = $form->getData();
            $this->app['em']->remove($form_data['liborder']);
            $this->app['em']->flush();

            return $this->app['twig']->render(
                'success.html.twig',
                array(
                    'message' => self::SUCCESS,
                )
            );

        } else {
            return $this->app['twig']->render(
                'finish_liborder.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }
}
