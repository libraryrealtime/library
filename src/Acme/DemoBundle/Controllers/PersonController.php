<?php

namespace Acme\DemoBundle\Controllers;

use Acme\DemoBundle\Forms\ChoosePersonType;
use Acme\DemoBundle\Forms\DeleteCopyType;
use \DateTime;
use \Exception;
use Acme\DemoBundle\Entities\Book;
use Acme\DemoBundle\Entities\Copy;
use Acme\DemoBundle\Entities\Order;
use Acme\DemoBundle\Entities\Person;
use Symfony\Component\Form\FormEvent;
use Acme\DemoBundle\Entities\Liborder;
use Symfony\Component\Form\FormEvents;
use Acme\DemoBundle\Entities\Position;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Acme\DemoBundle\Forms\PositionType;
use Acme\DemoBundle\Forms\LiborderType;
use Acme\DemoBundle\Forms\PersonType;
use Acme\DemoBundle\Forms\BookType;
use Acme\DemoBundle\Forms\DeleteBookType;
use Acme\DemoBundle\Forms\CopyType;
use Acme\DemoBundle\Forms\AuthorType;
use Acme\DemoBundle\Forms\DeletePersonType;
use Acme\DemoBundle\Forms\FinLiborderType;
use Symfony\Component\Form\FormError;
use  Acme\DemoBundle\Forms\ChoosePositionType;
use  Acme\DemoBundle\Forms\DeletePositionType;

class PersonController
{
    const CARDNUMBER_ALLREADY_EXISTS = "В системе уже зарегистрирован человек с таким номером абонемента.";
    const POSITION_ALLREADY_EXISTS   = "Должность с таким названием уже существует";
    const PERSON_NOT_FOUND_ERROR     = "Не найдена информация о человеке с данным идентификатором";
    const AUTHOR_NOT_FOUND_ERROR     = "Не найдена информация об атворе с данным идентификатором";
    const AUTHOR_ALLREADY_EXISTS     = "Автор с такими именем и фамилей уже существуют в системе";
    const POSITION_NOT_FOUND_ERROR   = "Не найдена информация о должности с данным идентификатором";
    const CAN_NOT_DELETE_AUTHOR      = "Невозможно удалить информацию о выбранном человеке, так как он является автором одной или нескольких книг";
    const CAN_NOT_DELETE_ABONENT     = "Невозможно удалить информацию о выбранном человеке, так как у него на руках есть невозвращенные книги";
    const DELETE_AUTHOR_SUCCEDED     = "Автор успешно удален из системы";
    const PERSON_DELETE_SUCCEDED     = "Абонета успешно удален из системы";
    const ADD_AUTHOR_SUCCESS         = "Информация об авторе успешно добавлена в систему";
    const EDIT_PERSON_SUCCESS        = "Информация об абоненте успешно изменена";
    const EDIT_AUTHOR_SUCCESS        = "Информация об авторе успешно изменена";
    const ADD_PERSON_SUCCESS         = 'Информация успешно занесена в базу данных';
    const POSITION_SUCCESS           = "Информация о должности успешно занесена в базу данных";
    const CAN_NOT_DELETE_POSITION    = "Невозможно удалить информацию о выбранной должности, так как она присвоена одному или нескольким абонентам";
    const POSITION_AUTHOR            = "Невозможно изменить или удалить должность \"Автор\"";
    const POSITION_DELETE_SUCCEDED   = 'Информация о доложности успешно удалена.';


    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }


    public function get_persons_with_books()
    {

        $query = $this->app['em']->createQuery('SELECT o
                                              FROM Acme\DemoBundle\Entities\Liborder o JOIN o.person p
                                               ORDER BY p.surname');

        $orders_on_hands = $query->getResult();

        return $this->app['twig']->render(
            'persons_with_books.html.twig',
            array(
                'orders_on_hands' => $orders_on_hands,
            )
        );
    }


    public function get_persons()
    {

        $query_persons = $this->app['em']->createQuery('SELECT p FROM Acme\DemoBundle\Entities\Person p
                                                                WHERE p.cardnumber IS NOT NULL');


        $abonents = $query_persons->getResult();


        $form_person = $this->app['form.factory']->createBuilder(new PersonType($this->app['url_generator'], $this->app['em'], false), new Person())->getForm();
        $form_position = $this->app['form.factory']->createBuilder(new PositionType($this->app['url_generator']), new Position())->getForm();

        return $this->app['twig']->render(
            'persons.html.twig',
            array(
                'persons' => $abonents,
                'add_person_form' => $form_person->createView(),
                'add_position_form' => $form_position->createView(),
            )
        );
    }


    public function PositionExists(Position $position) {

        if ($position->getId() != null) {

            $query = $this->app['em']->createQuery('SELECT p FROM Acme\DemoBundle\Entities\Position p WHERE LOWER(p.title) = LOWER( :title)');

            $query->setParameter("title",   $position->getTitle());

            $position_from_db = $query->getOneOrNullResult();

            if ($position_from_db == null) {
                return false;
            } else {
                if ($position->getId() == $position_from_db->getId()) {
                    //то это редактирование существующией. все хорошо
                    return false;
                } else {
                    //это есть редактирование сущности, при этом указан cn другой сущности, уже занесенной в бд
                    return true;
                }
            }
        } else {
            //а это проверка для добавления
            $query = $this->app['em']->createQuery('SELECT COUNT(p) FROM Acme\DemoBundle\Entities\Position p WHERE LOWER(p.title) = LOWER( :title)');

            $query->setParameter("title",   $position->getTitle());
            $count = $query->getSingleScalarResult();

            if ($count > 0) {
                return true;
            }   else {
                return false;
            }
        }
    }

    public function add_position(Request $request)
    {
        $position_object = new Position();
        $form_postion = $this->app['form.factory']->createBuilder(new PositionType($this->app['url_generator']), $position_object)->getForm();
        $form_postion->handleRequest($request);

        if ($form_postion->isValid()) {
            if ( ! $this->PositionExists($position_object)) {
                $this->app['em']->persist($position_object);
                $this->app['em']->flush();
                return $this->app['twig']->render('success.html.twig', array(
                    'message' => self::POSITION_SUCCESS,
                ));
            } else {
                // такая должность уже есть
                // добавить ошибку, далее в форму и вернуть ее

                $form_postion->get('title')->addError(new FormError(self::POSITION_ALLREADY_EXISTS));
                return $this->app['twig']->render(
                    'add_position.html.twig',
                    array(
                        'form' => $form_postion->createView(),
                    )
                );
            }
        } else {
            return $this->app['twig']->render(
                'add_position.html.twig',
                array(
                    'form' => $form_postion->createView(),
                )
            );
        }
    }

    private function IsPersonExists(Person $person){

        if ( $person->getId() != null ) {
            // это изменение существующего абонента

            $query = $this->app['em']->createQuery("SELECT p FROM Acme\DemoBundle\Entities\Person p WHERE LOWER(p.cardnumber) = LOWER(:cardnumber)");

            $query->setParameter("cardnumber", $person->getCardnumber());

            $person_from_db = $query->getOneOrNullResult();

            if ($person_from_db == null) {
                return false;
            } else {
                if ($person->getId() == $person_from_db->getId()) {
                    //то это редактирование существующией. все хорошо
                    return false;
                } else {
                    //это есть редактирование сущности, при этом указан cn другой сущности, уже занесенной в бд
                    return true;
                }
            }
        }else {
            //а это проверка для добавления
            // проверка уникальности cardnumber
            $query = $this->app['em']->createQuery("SELECT COUNT(p) FROM Acme\DemoBundle\Entities\Person p WHERE LOWER(p.cardnumber) = LOWER(:cardnumber)");
            $query->setParameter("cardnumber", $person->getCardnumber());
            $count = $query->getSingleScalarResult();

            if ($count > 0){
                return true;
            } else {
                return false;
            }
        }
    }

    public function add_person(Request $request)
    {
        $person = new Person();

        $form_person = $this->app['form.factory']->createBuilder(new PersonType($this->app['url_generator'], $this->app['em'], false, false), $person)->getForm();
        $form_person->handleRequest($request);

        if ($form_person->isValid()) {
            if (  !  $this->IsPersonExists($person) ){
                $this->app['em']->persist($person);
                $this->app['em']->flush();

                return $this->app['twig']->render('success.html.twig',array(
                    'message' =>  self::ADD_PERSON_SUCCESS,
                ));
            } else {

                $form_person->get('cardnumber')->addError(new FormError(self::CARDNUMBER_ALLREADY_EXISTS));

                return $this->app['twig']->render(
                    'add_person.html.twig',
                    array(
                        'form' => $form_person->createView(),
                    )
                );

            }
        } else {
            return $this->app['twig']->render(
                'add_person.html.twig',
                array(
                    'form' => $form_person->createView(),
                )
            );
        }
    }

    public function get_authors()
    {

        $authors = $this->app['em']->createQuery("SELECT p
                                              FROM Acme\DemoBundle\Entities\Person p
                                              JOIN p.positions pos WHERE pos.title = 'Автор'");

        $authors_result = $authors->getResult();

        return $this->app['twig']->render(
            'authors.html.twig',
            array(
                'authors' => $authors_result,
            )
        );
    }

    public function get_person(Request $request, $person_id){

        $person = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Person")->find($person_id);

        if ($person) {
            return $this->app['twig']->render(
                'person.html.twig',
                array(
                    'person' => $person,
                )
            );
        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::PERSON_NOT_FOUND_ERROR,
                )
            );
        }
    }

    public function get_position(Request $request, $position_id) {

        $position = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Position")->find($position_id);

        if ($position) {
            return $this->app['twig']->render(
                'position.html.twig',
                array(
                    'position' => $position,
                )
            );
        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::POSITION_NOT_FOUND_ERROR,
                )
            );
        }
    }
    private function isAuthorExists(Person $author_person)
    {

        if ($author_person->getId() != null) {

            $query = $this->app['em']->createQuery('SELECT p FROM Acme\DemoBundle\Entities\Person p WHERE p.cardnumber IS NULL AND LOWER(p.name) = LOWER(:name) AND LOWER(p.surname) = LOWER(:surname)');

            $query->setParameter("name", $author_person->getName());
            $query->setParameter("surname", $author_person->getSurname());

            $author_from_db = $query->getOneOrNullResult();

            if ($author_from_db == null) {
                return false;
            } else {
                if ($author_person->getId() == $author_from_db->getId()) {
                    //то это редактирование существующией. все хорошо
                    return false;
                } else {
                    //это есть редактирование сущности, при этом указан cn другой сущности, уже занесенной в бд
                    return true;
                }
            }
        } else {
            //а это проверка для добавления
            $query = $this->app['em']->createQuery('SELECT COUNT(p) FROM Acme\DemoBundle\Entities\Person p WHERE p.cardnumber IS NULL AND LOWER(p.name) = LOWER(:name) AND LOWER(p.surname) = LOWER(:surname)');

            $query->setParameter("name", $author_person->getName());
            $query->setParameter("surname", $author_person->getSurname());

            $count = $query->getSingleScalarResult();

            if ($count > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function add_author(Request $request)
    {
        $author_person = new Person();
        $form = $this->app['form.factory']->createBuilder(new PersonType($this->app['url_generator'], $this->app['em'], true, false), $author_person)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            // Есть ли в системе должность автора.
            $author_position_query = $this->app['em']->createQuery("SELECT p
                                                  FROM Acme\DemoBundle\Entities\Position p
                                                  WHERE p.title = 'Автор' ");

            $author_position = $author_position_query->getOneOrNullResult();

            if ($author_position) {

                if ( $this->isAuthorExists($author_person) ) {

                    $form->addError( new FormError( self::AUTHOR_ALLREADY_EXISTS));

                    return $this->app['twig']->render(
                        'add_author.html.twig',
                        array(
                            'form' => $form->createView(),
                        )
                    );
                } else {
                    // добавить эту position к author
                    $author_person->getPositions()->add($author_position);
                    $this->app['em']->persist($author_person);
                    $this->app['em']->flush();

                    return $this->app['twig']->render(
                        'success.html.twig',
                        array(
                            'message' => self::ADD_AUTHOR_SUCCESS,
                        )
                    );
                }

            } else {
                // если ее нет - то создать
                $new_author_position = new Position();
                $new_author_position->setTitle("Автор");
                $this->app['em']->persist($new_author_position);
                //найти ее и добавить
                $author_person->getPositions()->add($new_author_position);
                $this->app['em']->persist($author_person);
                $this->app['em']->flush();

                return $this->app['twig']->render(
                    'success.html.twig',
                    array(
                        'message' => self::ADD_AUTHOR_SUCCESS,
                    )
                );
            }

        } else {
            return $this->app['twig']->render(
                'add_author.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function choose_author_for_edit(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(new ChoosePersonType($this->app['em'], true))->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $person = $form->getData()['person'];
            $string_id = (string)$person->getId();
            if ( $person->getCardnumber() == null) {
                // то редирект на автора
                return $this->app->redirect($this->app['url_generator']->generate('edit_author', array('author_id' => $string_id) ));
            } else {
                //редирект на персону
                return $this->app->redirect($this->app['url_generator']->generate('edit_person', array('person_id' => $string_id) ));
            }
        } else {
            return $this->app['twig']->render(
                'choose_author.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function choose_person_for_edit(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(new ChoosePersonType($this->app['em'], false))->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $author = $form->getData();
            $string_id = (string)$author['person']->getId();
            return $this->app->redirect($this->app['url_generator']->generate('edit_person', array('person_id' => $string_id) ));
        } else {
            return $this->app['twig']->render(
                'choose_person.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function choose_author_for_delete(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(new ChoosePersonType($this->app['em'], true))->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $author = $form->getData();
            $author_id = (string)$author['person']->getId();
            return $this->app->redirect($this->app['url_generator']->generate('delete_author', array('author_id' => $author_id) ));
        } else {

            // далее
            return $this->app['twig']->render(
                'choose_author.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function choose_person_for_delete(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(new ChoosePersonType($this->app['em'], false))->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {

            $author = $form->getData();
            $string_id = (string)$author['person']->getId();

            return $this->app->redirect($this->app['url_generator']->generate('delete_person', array('person_id' => $string_id) ));
        } else {

            return $this->app['twig']->render(
                'choose_author.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function edit_person(Request $request, $person_id)
    {
        // выдать форму со списком всех тех  у кого есть поле автора
        $person = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Person")->find($person_id);

        if ($person) {
            // Выпадает или уже есть форма
            $form = $this->app['form.factory']->createBuilder(new PersonType($this->app['url_generator'], $this->app['em'], false), $person)->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {
                if ( ! $this->IsPersonExists($person)){
                    $this->app['em']->flush();

                    return $this->app['twig']->render(
                        'success.html.twig',
                        array(
                            'message' => self::EDIT_PERSON_SUCCESS,
                        )
                    );
                } else {

                    $form->get('cardnumber')->addError(new FormError(self::CARDNUMBER_ALLREADY_EXISTS));

                    return $this->app['twig']->render(
                        'edit_person.html.twig',
                        array(
                            'form' => $form_person->createView(),
                        )
                    );

                }
            } else {
                return $this->app['twig']->render(
                    'edit_person.html.twig',
                    array(
                        'form' => $form->createView(),
                    )
                );
            }
        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::PERSON_NOT_FOUND_ERROR,
                )
            );
        }
    }

    public function edit_author(Request $request, $author_id)
    {
        // выдать форму со списком всех тех  у кого есть поле автора
        $author = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Person")->find($author_id);

        if ($author) {
            // Выпадает или уже есть форма
            $form = $this->app['form.factory']->createBuilder(new PersonType($this->app['url_generator'], $this->app['em'], true, true), $author)->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {
                if ( $this->isAuthorExists($author) ) {

                    $form->addError(new FormError(self::AUTHOR_ALLREADY_EXISTS));

                    return $this->app['twig']->render(
                        'edit_author.html.twig',
                        array(
                            'form' => $form->createView(),
                        )
                    );

                } else {
                    $this->app['em']->flush();
                    $string_id = (string)$author->getId();
                    return $this->app['twig']->render(
                        'success.html.twig',
                        array(
                            'message' => self::EDIT_AUTHOR_SUCCESS,
                        )
                    );
                }
            } else {
                return $this->app['twig']->render(
                    'edit_author.html.twig',
                    array(
                        'form' => $form->createView(),
                    )
                );
            }
        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::AUTHOR_NOT_FOUND_ERROR,
                )
            );
        }
    }


    public function delete_author(Request $request, $author_id){

        $author = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Person")->find($author_id);

        if ($author) {

                $query = $this->app['em']->createQuery('SELECT COUNT (b) FROM Acme\DemoBundle\Entities\Book b  JOIN b.authors author WHERE author.id = :author_id');
                $query->setParameter("author_id", $author_id);
                $count = $query->getSingleScalarResult();
                if ($count == 0) {
                    $form = $this->app['form.factory']->createBuilder(new DeletePersonType())->getForm();
                    $form->handleRequest($request);

                    if ($form->isValid()) {

                        $this->app['em']->remove($author);
                        $this->app['em']->flush();

                        return $this->app['twig']->render(
                            'success.html.twig',
                            array(
                                'message' => self::DELETE_AUTHOR_SUCCEDED,
                            )
                        );

                    } else {
                        return $this->app['twig']->render(
                            'delete_author.html.twig',
                            array(
                                'form' => $form->createView(),
                            )
                        );
                    }
                } else {
                    return $this->app['twig']->render(
                        'error.html.twig',
                        array(
                            'error' => self::CAN_NOT_DELETE_AUTHOR,
                        )
                    );
                }

        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::AUTHOR_NOT_FOUND_ERROR,
                )
            );
        }
    }


    public function delete_person(Request $request, $person_id)
    {

        $person = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Person")->find($person_id);

        if ($person) {


            $query = $this->app['em']->createQuery('SELECT COUNT (l) FROM Acme\DemoBundle\Entities\Liborder l WHERE (
                                                    l.person = :person_id)');

            $query->setParameter("person_id", $person_id);
            $count = $query->getSingleScalarResult();


            if ($count == 0) {

                $query = $this->app['em']->createQuery('SELECT COUNT (b) FROM Acme\DemoBundle\Entities\Book b  JOIN b.authors author WHERE author.id = :person_id');

                $query->setParameter("person_id", $person_id);
                $count = $query->getSingleScalarResult();

                if ($count == 0) {
                    $form = $this->app['form.factory']->createBuilder(new DeletePersonType())->getForm();
                    $form->handleRequest($request);

                    if ($form->isValid()) {

                        $this->app['em']->remove($person);
                        $this->app['em']->flush();

                        return $this->app['twig']->render(
                            'success.html.twig',
                            array(
                                'message' => self::PERSON_DELETE_SUCCEDED,
                            )
                        );

                    } else {
                        return $this->app['twig']->render(
                            'delete_author.html.twig',
                            array(
                                'form' => $form->createView(),
                            )
                        );
                    }
                } else { // есть книги с авторством данного человека. Продолжить никак нельзя.
                    return $this->app['twig']->render(
                        'error.html.twig',
                        array(
                            'error' => self::CAN_NOT_DELETE_AUTHOR,
                        )
                    );
                }
            } else { // есть книги на руках
                return $this->app['twig']->render(
                    'error.html.twig',
                    array(
                        'error' => self::CAN_NOT_DELETE_ABONENT,
                    )
                );
            }

        } else { // не сущестует $person
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::PERSON_NOT_FOUND_ERROR,
                )
            );
        }
    }


    public function edit_position(Request $request, $position_id)
    {
        $position = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Position")->find($position_id);

        if ($position) {
            $form = $this->app['form.factory']->createBuilder(new PositionType($this->app['url_generator']), $position)->getForm();
            $form->handleRequest($request);

            if ($form->isValid()) {
                if ( ! $this->PositionExists($position)) {
                    if ( $position->getTitle() ==  'Автор' ) {
                        return $this->app['twig']->render(
                            'error.html.twig',
                            array(
                                'error' => self::POSITION_AUTHOR,
                            )
                        );
                    } else {
                        $this->app['em']->flush();

                        return $this->app['twig']->render(
                            'success.html.twig',
                            array(
                                'message' => self::POSITION_SUCCESS,
                            )
                        );
                    }
                } else {

                    $form->get('title')->addError(new FormError(self::POSITION_ALLREADY_EXISTS));

                    return $this->app['twig']->render(
                        'edit_position.html.twig',
                        array(
                            'form' => $form->createView(),
                        )
                    );
                }
            } else {
                return $this->app['twig']->render(
                    'edit_position.html.twig',
                    array(
                        'form' => $form->createView(),
                    )
                );
            }
        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::POSITION_NOT_FOUND_ERROR,
                )
            );
        }
    }

    public function choose_position_for_edit(Request $request)

    {
        $form = $this->app['form.factory']->createBuilder(new ChoosePositionType($this->app['em'], false))->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $string_id = (string)$data['position']->getId();


            return $this->app->redirect($this->app['url_generator']->generate('edit_position', array('position_id' =>$string_id) ));
        } else {

            return $this->app['twig']->render(
                'choose_position.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function choose_position_for_delete(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(new ChoosePositionType($this->app['em'], false))->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $string_id = (string)$data['position']->getId();

            return $this->app->redirect($this->app['url_generator']->generate('delete_position', array('position_id' =>$string_id) ));
        } else {

            return $this->app['twig']->render(
                'choose_position.html.twig',
                array(
                    'form' => $form->createView(),
                )
            );
        }
    }

    public function delete_position(Request $request, $position_id)
    {
        $position = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Position")->find($position_id);

        if ($position) {
            $query = $this->app['em']->createQuery('SELECT COUNT (p) FROM Acme\DemoBundle\Entities\Person p JOIN p.positions pos WHERE pos.id = :position_id ');

            $query->setParameter("position_id", $position_id);
            $count = $query->getSingleScalarResult();

            if ($count == 0) {
                    if ( $position->getTitle() ===  'Автор') {
                        return $this->app['twig']->render(
                            'error.html.twig',
                            array(
                                'error' => self::POSITION_AUTHOR,
                            )
                        );
                    } else {
                        $form = $this->app['form.factory']->createBuilder(new DeletePositionType())->getForm();
                        $form->handleRequest($request);

                        if ($form->isValid()) {

                            $this->app['em']->remove($position);
                            $this->app['em']->flush();

                            return $this->app['twig']->render(
                                'success.html.twig',
                                array(
                                    'message' => self::POSITION_DELETE_SUCCEDED,
                                )
                            );

                        } else {
                            return $this->app['twig']->render(
                                'delete_position.html.twig',
                                array(
                                    'form' => $form->createView(),
                                )
                            );
                        }
                    }
                } else {
                    return $this->app['twig']->render(
                        'error.html.twig',
                        array(
                            'error' => self::CAN_NOT_DELETE_POSITION,
                        )
                    );
                }

        } else {
            return $this->app['twig']->render(
                'error.html.twig',
                array(
                    'error' => self::POSITION_NOT_FOUND_ERROR,
                )
            );
        }
    }


    public function get_positions(Request $request){

        $positions = $this->app['em']->getRepository("Acme\DemoBundle\Entities\Position")->findAll();

        return $this->app['twig']->render(
            'positions.html.twig',
            array(
                'positions' => $positions,
            )
        );

    }

}