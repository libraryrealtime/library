<?php

namespace Acme\DemoBundle\Forms;

use Doctrine\ORM\EntityManager;
use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ChoosePersonType extends AbstractType{

    protected $em;
    protected $is_authors_only;

    public function __construct(EntityManager $em, $is_authors_only = false)
    {
        $this->em = $em;
        $this->is_authors_only = $is_authors_only;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){


        $persons = null;
        if ($this->is_authors_only) {
            $query_authors = $this->em->createQuery("SELECT p
                                              FROM Acme\DemoBundle\Entities\Person p
                                              JOIN p.positions pos WHERE pos.title = 'Автор'");

            $persons = $query_authors->getResult();

        } else {
            $query_authors = $this->em->createQuery("SELECT p
                                              FROM Acme\DemoBundle\Entities\Person p WHERE p.cardnumber IS NOT NULL" );

            $persons = $query_authors->getResult();

        }

        $builder
            ->add(
                'person','entity', array(
                'label'         =>  'Человек',
                'class'         =>  'Acme\DemoBundle\Entities\Person',
                'choices'       =>  $persons,
                'em'            =>  $this->em,
                'constraints'   => array(new Assert\NotBlank()),
                'attr'          =>  array(
                    'class'     => 'person_select')
                )
            )
            ->add('Далее', 'submit', array(
                    'attr' => array(
                        'class'     => 'btn btn-default')
                )
            )
            ->setMethod('GET');

    }
    public function getName(){
        return 'choose_person';
    }
}
