<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CopyType extends AbstractType{

    protected $is_for_edit;

    public function __construct($is_for_edit = false)
    {
        $this->is_for_edit = $is_for_edit;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('innerNumber', 'text', array(
                'label' => 'Номер копии',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'constraints' => array(new Assert\NotBlank(),  new Assert\Length(array('min' => 2, 'max' => 200)))))
            ->add('Сохранить', 'submit', array(
                    'attr' => array(
                        'class' => 'btn btn-default')
                )
            );

             if ($this->is_for_edit) {
                 $builder->setMethod('PUT');
             } else {
                 $builder->setMethod('POST');
             }
    }
    public function getName(){
        if ($this->is_for_edit) {
            return 'copy_edit';
        } else {
            return 'copy';
        }
    }
}
