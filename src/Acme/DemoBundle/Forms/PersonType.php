<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PersonType extends AbstractType{

    protected $em;
    protected $is_for_edit;
    protected $is_for_author;
    protected $url_generator;

    public function __construct($url_generator, $em, $is_for_author = false, $is_for_edit = false){

        $this->em = $em;

        $this->is_for_edit  = $is_for_edit;
        $this->is_for_author = $is_for_author;
        $this->url_generator = $url_generator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        if ( ! $this->is_for_author) {
            $builder
                ->add('positions','entity', array(
                        'label'         => 'Должность',
                        'class'         => 'Acme\DemoBundle\Entities\Position',
                        'placeholder'   => 'Choose an option',
                        'multiple'      => true,
                        'em'            => $this->em,
                        'constraints'   => array(new Assert\NotBlank()),
                        'attr'          => array(
                            'class'     => 'position_select'
                        )
                    )
                )

                ->add('cardnumber', 'text', array(
                        'label' => 'Номер билета',
                        'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max' => 200))),
                        'attr'          => array(
                            'class'     => 'form-control'
                        )
                    )
                );
        }

        $builder
            ->add('name','text', array(
                    'label' => 'Имя',
                    'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max' => 200))),
                    'attr'          => array(
                        'class'     => 'form-control'
                    )
                )
            )
            ->add('surname', 'text', array(
                    'label' => 'Фамилия',
                    'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max' => 200))),
                    'attr'          => array(
                        'class'     => 'form-control'
                    )
                )
            )

            ->add('Сохранить', 'submit', array(
                    'attr' => array(
                        'class'     => 'btn btn-default')
                )
            )
            ->setMethod('POST');
    }
    public function getName(){
        return 'person';
    }
}
