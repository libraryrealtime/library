<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AuthorType extends AbstractType{

    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){


        $builder
            ->add('name','text', array(
                    'label'         => 'Имя',
                    'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5, 'max' => 200))),
                    'attr'          => array(
                        'class'     => 'form-control'
                    )
                )
            )
            ->add('surname', 'text',array(
                    'label'         => 'Фамилия',
                    'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5, 'max' => 200))),
                    'attr'          => array(
                        'class'     => 'form-control'
                    )
                )
            )

            ->add('Сохранить', 'submit', array(
                'attr' => array(
                    'class'     => 'btn btn-default')
                )
            )
            ->setAction($this->app['url_generator']->generate('add_author'))
            ->setMethod('POST');


    }
    public function getName(){
        return 'author';
    }
}
