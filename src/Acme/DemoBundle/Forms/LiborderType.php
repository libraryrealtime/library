<?php
namespace Acme\DemoBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvents;
use Acme\DemoBundle\Entities\Book;
use \DateTime;

class LiborderType extends AbstractType{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now =  new DateTime('now');
        $not_now =  new DateTime('now');
        $not_now = $not_now->modify('+1 year');

       $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $event->stopPropagation();
        }, 250); // Always set a higher priority than ValidationListener

        $query_persons = $this->em->createQuery('SELECT p
                                          FROM Acme\DemoBundle\Entities\Person p
                                          WHERE (
                                            (
                                              SELECT COUNT(o)
                                              FROM Acme\DemoBundle\Entities\Liborder o
                                              WHERE (
                                                o.person = p
                                              )
                                            ) < 5 AND p.cardnumber IS NOT NULL)');

        $persons = $query_persons->getResult();
        $books =  $this->em->getRepository("Acme\DemoBundle\Entities\Book")->findAll();


          $formModifier = function (FormInterface $form, Book $book = null, $em) {

              $copies = array();

              if ($book) {
                  $copies_query = $em->createQuery('SELECT c FROM Acme\DemoBundle\Entities\Copy c WHERE c.book = :book_id
                                                          AND (SELECT COUNT(l) FROM Acme\DemoBundle\Entities\Liborder l where l.copy = c ) = 0 ');

                  $copies_query->setParameter("book_id", $book->getId());
                  $copies = $copies_query->getResult();

              }

              $form->add(
                  'copy', 'entity', array(
                      'label' => 'Копия',
                      'class' => 'Acme\DemoBundle\Entities\Copy',
                      'choices' => $copies,
                      'em' => $em,
                      'placeholder' => 'Выберите копию',
                      'constraints' => array(new Assert\NotBlank()),
                      'attr' => array(
                          'class' => 'copy_select'
                      )
                  )
              );

          };


        $builder
            ->add(
                'person','entity', array(
                    'label'         =>  'Человек',
                    'class'         =>  'Acme\DemoBundle\Entities\Person',
                    'choices'       =>  $persons,
                    'em'            =>  $this->em,
                    'placeholder'   =>  'Выберите человека',
                    'constraints'   => array(new Assert\NotBlank()),
                    'attr'          => array(
                        'class'     => 'person_select'
                    )
            ))

            ->add(
                'book', 'entity', array(
                    'label'         => 'Книга',
                    'class'         => 'Acme\DemoBundle\Entities\Book',
                    'choices'       => $books,
                    'em'            => $this->em,
                    'placeholder'   => 'Выберите книгу',
                    'constraints'   => array(new Assert\NotBlank()),
                    'attr'          => array(
                        'class'     => 'book_select'
                    )
            ))

            ->add('findate', 'date', array(
                    'label'         => 'Конечная дата приема',
                    'years' => range( $now->format('Y'), $not_now->format('Y')),
            ))
            ->add('Добавить заявку', 'submit',
                array(
                    'label' => 'Создать заявку',
                    'attr' => array(
                        'class'     => 'btn btn-default')
                )
            )
            ->setMethod('POST');

           $em = $this->em ;

            $builder->addEventListener(
                 FormEvents::PRE_SET_DATA,
                 function (FormEvent $event) use ($formModifier, $em) {
                     $data = $event->getData(); //liborder
                     $formModifier($event->getForm(), $data->getBook(), $em);
                 }
             );

             $builder->get('book')->addEventListener(
                 FormEvents::POST_SUBMIT,
                 function (FormEvent $event) use ($formModifier, $em) {
                    $book = $event->getForm()->getData();
                    $formModifier($event->getForm()->getParent(), $book, $em);

                 }
             );

    }

    public function getName()
    {
        return 'liborder';
    }
}