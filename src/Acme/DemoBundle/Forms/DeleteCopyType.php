<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class DeleteCopyType extends AbstractType{



    public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->setMethod('DELETE')
            ->add('Удалить', 'submit', array(
                'attr' => array('class' => 'btn btn-default delete')
            ));
    }
    public function getName(){
        return 'delete_copy';
    }
}

