<?php

namespace Acme\DemoBundle\Forms;

use Doctrine\ORM\EntityManager;
use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ChoosePositionType extends AbstractType{

    protected $em;

    public function __construct(EntityManager $em, $is_authors_only = false)
    {
        $this->em = $em;
        $this->is_authors_only = $is_authors_only;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

       $query_positions= $this->em->createQuery('SELECT p FROM Acme\DemoBundle\Entities\Position p WHERE LOWER(p.title) != LOWER(:title)');
       $query_positions->setParameter('title','Автор');
       $positions = $query_positions->getResult();

       $builder
            ->add(
                'position','entity', array(
                    'label'         =>  'Должность',
                    'class'         =>  'Acme\DemoBundle\Entities\Position',
                    'choices'       =>  $positions,
                    'em'            =>  $this->em,
                    'constraints'   => array(new Assert\NotBlank()),
                    'attr'          =>  array(
                        'class'     => 'position_select')
                )
            )
            ->add('Далее', 'submit', array(
                    'attr' => array(
                        'class'     => 'btn btn-default')
                )
            )
            ->setMethod('GET');

    }
    public function getName(){
        return 'choose_position';
    }
}
