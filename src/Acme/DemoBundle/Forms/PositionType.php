<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PositionType extends AbstractType{

    protected $url_generator;

    public function __construct($url_generator){
        $this->url_generator = $url_generator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('title', 'text', array(
                'label' => 'Название',
                'attr'          => array(
                    'class'     => 'form-control'
                ),
                'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max' => 200))),
            ))

            ->add('Сохранить', 'submit',array(
                    'attr' => array(
                        'class'     => 'btn btn-default')
                )
            )
            ->setMethod('POST');
    }
    public function getName(){
        return 'position';
    }
}
