<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class DeletePositionType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->setMethod('POST')
            ->add('Удалить', 'submit', array(
                'attr' => array('class' => 'btn btn-default delete')
            ));
    }

    public function getName(){
        return 'delete_position';
    }
}
