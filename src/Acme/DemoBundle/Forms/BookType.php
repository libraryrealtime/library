<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BookType extends AbstractType{

    protected $em;
    protected $is_edit;
    protected $url_generator;

    public function __construct( $em, $url_generator, $is_edit = false)
    {
        $this->em = $em;
        $this->is_edit = $is_edit;
        $this->url_generator = $url_generator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $query_authors = $this->em->createQuery("SELECT p
                                              FROM Acme\DemoBundle\Entities\Person p
                                              JOIN p.positions pos WHERE pos.title = 'Автор'");
        $authors = $query_authors->getResult();

        $builder
            ->add(
                'authors','entity', array(
                    'label'         =>  'Авторы',
                    'class'         =>  'Acme\DemoBundle\Entities\Person',
                    'choices'       =>  $authors,
                    'multiple'      =>  true,
                    'em'            =>  $this->em,
                    'constraints'   => array(new Assert\NotBlank()),
                    'attr'          =>  array(
                        'class'   => 'author_select')
                )
            )
            ->add('title',  'text', array(
                'label'         => 'Название',
                'constraints'   => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max' => 200))),
                'attr'          => array(
                    'class'     => 'form-control')
                )
            )
            ->add('edition','text', array(
                'label' => 'Редакция',
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2, 'max' => 200))),
                'attr'          => array(
                    'class'     => 'form-control')
                )
            )
            ->add('volume', 'integer', array(
                'label' => 'Объем',
                'constraints' => array(new Assert\NotBlank(),  new Assert\Range(array('min' => 1, 'max' => 100000))),
                'attr'          => array(
                    'class'     => 'form-control')
                )
            )


            ->add('Сохранить',   'submit' , array(
                'attr' => array(
                    'class'     => 'btn btn-default')
                )
            );

        if ($this->is_edit) {

            $builder->setAction($this->url_generator->generate('edit_book', array('book_id' => '0')))->setMethod('PUT');
        }else {
            $builder->setAction($this->url_generator->generate('add_book'))->setMethod('POST');
        }
    }
    public function getName()
    {
        if ($this->is_edit) {
            return 'book_edit';
        } else {
            return 'book';
        }
    }
}
