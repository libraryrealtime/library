<?php

namespace Acme\DemoBundle\Forms;

use Silex\Application;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class FinLiborderType extends AbstractType{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){

        $query_liborders = $this->em->createQuery('SELECT o
                                          FROM Acme\DemoBundle\Entities\Liborder o');

        $liborders = $query_liborders->getResult();

        $builder
            ->setMethod('POST')
            ->add(
                'liborder', 'entity', array(
                    'label'         => 'Выданные книги',
                    'class'         => 'Acme\DemoBundle\Entities\Liborder',
                    'placeholder'   => 'Выберите заявку',
                    'choices'       => $liborders,
                    'em'            => $this->em,
                    'constraints'   => array(new Assert\NotBlank()),
                    'attr' => array(
                        'class'     => 'liborders_select'
                    )
            ))
            ->add('save', 'submit',array(
                'label' => 'Принять',
                'attr' => array(
                    'class'     => 'btn btn-default')
            ))

            ->setMethod('POST');
    }

    public function getName(){
        return 'finish_liborder';
    }
}