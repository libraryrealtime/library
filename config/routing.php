<?php
use \Exception;
use Acme\DemoBundle\Controllers\BookController;
use Acme\DemoBundle\Controllers\PersonController;
use Acme\DemoBundle\Controllers\LiborderController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app->error(function (Exception $e, $code) use ($app) {

    $app['monolog']->addError(sprintf("Exception message: '%s'.", $e->getMessage()));

    if ($app['debug']) {
        return;
    }

    $templates = array(
        'Errors/'.$code.'.html.twig',
        'Errors/'.substr($code, 0, 2).'x.html.twig',
        'Errors/'.substr($code, 0, 1).'xx.html.twig',
        'Errors/default.html',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

$app['book.controller']     = $app->share(function() use ($app) {
    return new BookController($app);
});
$app['person.controller']   = $app->share(function() use ($app) {
    return new PersonController($app);
});
$app['liborder.controller'] = $app->share(function() use ($app) {
    return new LiborderController($app);
});

//Показ списка всех книг и их копий
$app->get('/books',                                         'book.controller:get_books_and_copies')         ->bind('books');
//Добавить книгу AJAX REQUEST
$app->post(     '/books',                                   'book.controller:add_book')                     ->bind('add_book');
//Изменить книгу AJAX REQUEST
$app->put(      '/books/{book_id}',                         'book.controller:edit_book')                    ->bind('edit_book')         ->assert('book_id','\d+');
//Удалить книгу AJAX REQUEST
$app->delete(   '/books/{book_id}',                         'book.controller:delete_book')                  ->bind('delete_book')       ->assert('book_id','\d+');
//Добавить копию AJAX REQUEST
$app->post(     '/books/{book_id}/copies',                  'book.controller:add_copy')                     ->bind('add_copy')          ->assert('book_id','\d+');
//Изменить копию AJAX REQUEST
$app->put(      '/books/{book_id}/copies/{copy_id}',        'book.controller:edit_copy')                    ->bind('edit_copy')         ->assert('book_id','\d+')       ->assert('copy_id','\d+');
//Удалить копию AJAX REQUEST
$app->delete(   '/books/{book_id}/copies/{copy_id}',        'book.controller:delete_copy')                  ->bind('delete_copy')       ->assert('book_id','\d+')       ->assert('copy_id','\d+');

$app->get(      '/books/{book_id}',                         'book.controller:get_book')                     ->bind('book')              ->assert('book_id','\d+');
$app->get(      '/books/{book_id}/copies/{copy_id}',        'book.controller:get_copy')                     ->bind('copy')              ->assert('copy_id','\d+');

//Индекс
$app->get(      '/',                                        'person.controller:get_persons_with_books');
$app->get(      '/persons_with_books',                      'person.controller:get_persons_with_books')     ->bind('persons_with_books');
$app->get(      '/persons',                                 'person.controller:get_persons')                ->bind('persons');
$app->match(    '/add_author',                              'person.controller:add_author')                 ->bind('add_author')                                        ->method('GET|POST');
$app->match(    '/add_person',                              'person.controller:add_person')                 ->bind('add_person')                                        ->method('GET|POST');

$app->get(      '/choose_person_for_edit',                  'person.controller:choose_person_for_edit')     ->bind('choose_person_for_edit');
$app->get(      '/choose_person_for_delete',                'person.controller:choose_person_for_delete')   ->bind('choose_person_for_delete');
$app->match(    '/edit_person/{person_id}',                 'person.controller:edit_person')                ->bind('edit_person')       ->assert('person_id','\d+')     ->method('GET|POST');
$app->match(    '/delete_person/{person_id}',               'person.controller:delete_person')              ->bind('delete_person')     ->assert('person_id','\d+')     ->method('GET|POST');
$app->get(      '/authors',                                 'person.controller:get_authors')                ->bind('get_authors');
$app->match(    '/choose_author_for_edit',                  'person.controller:choose_author_for_edit')     ->bind('choose_author_for_edit')                            ->method('GET|POST');
$app->match(    '/choose_author_for_delete',                'person.controller:choose_author_for_delete')   ->bind('choose_author_for_delete')                          ->method('GET|POST');

$app->get(      '/authors/{person_id}',                     'person.controller:get_person')                 ->bind('author')            ->assert('person_id','\d+');
$app->get(      '/persons/{person_id}',                     'person.controller:get_person')                 ->bind('person')            ->assert('person_id','\d+');
$app->match(    '/edit_author/{author_id}',                 'person.controller:edit_author')                ->bind('edit_author')       ->assert('author_id','\d+')     ->method('GET|POST');
$app->match(    '/delete_author/{author_id}',               'person.controller:delete_author')              ->bind('delete_author')     ->assert('author_id','\d+')     ->method('GET|POST');

$app->get(      '/positions/{position_id}',                 'person.controller:get_position')               ->bind('position')          ->assert('position_id','\d+');
$app->match(    '/add_position',                            'person.controller:add_position')               ->bind('add_position')                                      ->method('GET|POST');
$app->match(    '/edit_position/{position_id}',             'person.controller:edit_position')              ->bind('edit_position')     ->assert('position_id','\d+')   ->method('GET|POST');
$app->match(    '/delete_position/{position_id}',           'person.controller:delete_position')            ->bind('delete_position')   ->assert('position_id','\d+')   ->method('GET|POST');

$app->get(      '/choose_position_for_edit',                'person.controller:choose_position_for_edit')   ->bind('choose_position_for_edit');
$app->get(      '/choose_position_for_delete',              'person.controller:choose_position_for_delete') ->bind('choose_position_for_delete');

$app->get(      '/positions',                               'person.controller:get_positions' )             ->bind('get_positions');

$app->match(    '/order',                                   'liborder.controller:new_order')                                                                            ->method('GET|POST');
$app->match(    '/finish_order',                            'liborder.controller:finish_order')                                                                         ->method('GET|POST');



