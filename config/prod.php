<?php
ini_set('display_errors', 'On');
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../src/Acme/DemoBundle/Views/',
    'twig.options' => array('debug' => false)
));