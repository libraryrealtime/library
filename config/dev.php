<?php

//

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_COMPILE_ERROR);
ini_set('display_errors', 'On');
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../src/Acme/DemoBundle/Views/',
    'twig.options' => array('debug' => true)
));

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\Debug\Debug;

ErrorHandler::register();
ExceptionHandler::register();

Debug::enable();
