<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;


$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/../src/Acme/DemoBundle/Entities"), $isDevMode, null, null, false);
$conn = array(
    'driver'    => 'pdo_pgsql',
    'host'      => 'localhost',
    'dbname'    => 'library',
    'user'      => 'mypguser',
    'password'  => 'qwer1234',
);

$entityManager = EntityManager::create($conn, $config);
