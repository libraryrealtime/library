<?php
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Acme\DemoBundle\Forms\Extensions\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Silex\Provider\MonologServiceProvider;

$app = new Application();

$app['em'] = $entityManager;

$app->register(new FormServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\SerializerServiceProvider());

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__ . '/../logs/default.log',
    'monolog.level' => "WARNING"
));

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale'                => 'ru',
    'translator.domains'    => array(),
));

$app->before(function () use ($app) {
    $app['translator']->addLoader('xlf', new Symfony\Component\Translation\Loader\XliffFileLoader());
    $app['translator']->addResource('xlf', __DIR__.'/../vendor/symfony/validator/Symfony/Component/Validator/Resources/translations/validators.ru.xlf', 'ru', 'validators');
});

$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    return $twig;
}));

$app['form.extensions'] = $app->share($app->extend('form.extensions', function ($extensions, $app) {
    $managerRegistry = new ManagerRegistry(null, array(), array('em'), null, null, '\Doctrine\ORM\Proxy\Proxy');
    $managerRegistry->setContainer($app);
    $extensions[] = new DoctrineOrmExtension($managerRegistry);
    return $extensions;
}));


return $app;