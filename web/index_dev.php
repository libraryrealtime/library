<?php

require_once        __DIR__ . '/../vendor/autoload.php';
//require_once        __DIR__ . '/../app/bootstrap.php';
require_once        __DIR__ . '/../app/bootstrap_dev.php';

$app = require_once __DIR__ . '/../app/app.php';
require             __DIR__ . '/../config/dev.php';
//require             __DIR__ . '/../config/prod.php';

require             __DIR__ . '/../config/routing.php';
$app->run();